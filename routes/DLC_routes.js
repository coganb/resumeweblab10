var express = require('express');
var router = express.Router();
var DLC_dal = require('../model/DLC_dal');
var Game_dal = require('../model/Game_dal');

// HERE
// View All companys
router.get('/all', function(req, res) {
    DLC_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('DLC/DLCViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.DLC_id == null) {
        res.send('DLC_id is null');
    }
    else {
        DLC_dal.getById(req.query.DLC_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('DLC/DLCViewById', {'result': result});
           }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    Game_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('DLC/DLCAdd', {'Game': result});
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.DLC_name == "") {
        res.send('DLC Name must be provided.');
    }
    else if(req.query.DLC_price == "")
    {
        res.send('DLC DLC_price must be provided.');
    }
    else if(req.query.Content_type == "")
    {
        res.send('DLC Content_type must be provided.');
    }
    else {
        // var game_name = DLC_dal.getName(req.query)[0][0];
        // passing all the query parameters (req.query) to the insert function instead of each individually
        DLC_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/DLC/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.DLC_id == null) {
        res.send('A DLC id is required');
    }
    else {
        DLC_dal.edit(req.query.DLC_id, function(err, result){
            res.render('DLC/DLCUpdate', {DLC: result[0][0]/*, address: result[1]*/});
        });
    }

});

/*
router.get('/edit2', function(req, res){
   if(req.query.company_id == null) {
       res.send('A company id is required');
   }
   else {
       company_dal.getById(req.query.company_id, function(err, company){
           address_dal.getAll(function(err, address) {
               res.render('company/companyUpdate', {company: company[0], address: address});
           });
       });
   }

});
*/


router.get('/update', function(req, res) {
    DLC_dal.update(req.query, function(err, result){
       res.redirect(302, '/DLC/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.DLC_id == null) {
        res.send('DLC_id is null');
    }
    else {
         DLC_dal.delete(req.query.DLC_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/DLC/all');
             }
         });
    }
});

module.exports = router;
