var express = require('express');
var router = express.Router();
var Character_dal = require('../model/Character_dal');
var Game_dal = require('../model/Game_dal');

// HERE
// View All companys
router.get('/all', function(req, res) {
    Character_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('Character/CharacterViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.character_id == null) {
        res.send('character_id is null');
    }
    else {
        Character_dal.getById(req.query.character_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('Character/CharacterViewById', {'result': result});
           }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    Game_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('Character/CharacterAdd', {'Game': result});
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.Character_name == "") {
        res.send('Character Name must be provided.');
    }
    else if(req.query.is_playable == "")
    {
        res.send('Character is_playable must be provided.');
    }
    else if(req.query.is_killable == "")
    {
        res.send('Character is_killable must be provided.');
    }
    else {
        // var game_name = Character_dal.getName(req.query)[0][0];
        // passing all the query parameters (req.query) to the insert function instead of each individually
        Character_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/Character/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.character_id == null) {
        res.send('A Character id is required');
    }
    else {
        Character_dal.edit(req.query.character_id, function(err, result){
            res.render('Character/CharacterUpdate', {Character: result[0][0]/*, address: result[1]*/});
        });
    }

});

/*
router.get('/edit2', function(req, res){
   if(req.query.company_id == null) {
       res.send('A company id is required');
   }
   else {
       company_dal.getById(req.query.company_id, function(err, company){
           address_dal.getAll(function(err, address) {
               res.render('company/companyUpdate', {company: company[0], address: address});
           });
       });
   }

});
*/


router.get('/update', function(req, res) {
    Character_dal.update(req.query, function(err, result){
       res.redirect(302, '/Character/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.character_id == null) {
        res.send('character_id is null');
    }
    else {
         Character_dal.delete(req.query.character_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/Character/all');
             }
         });
    }
});

module.exports = router;
