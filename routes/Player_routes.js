var express = require('express');
var router = express.Router();
var Player_dal = require('../model/Player_dal');
// var email_dal = require('../model/email_dal');

// HERE
// View All companys
router.get('/all', function(req, res) {
    Player_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('Player/PlayerViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.player_id == null) {
        res.send('player_id is null');
    }
    else {
        Player_dal.getById(req.query.player_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('Player/PlayerViewById', {'result': result});
           }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    Player_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('Player/PlayerAdd', {'email': result});
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.email == "") {
        res.send('Player Name must be provided.');
    }
    else if(req.query.preffered_genre == "")
    {
        res.send('Player preffered_genre must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        Player_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/Player/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.player_id == null) {
        res.send('A Player id is required');
    }
    else {
        Player_dal.edit(req.query.player_id, function(err, result){
            res.render('Player/PlayerUpdate', {Player: result[0][0]/*, address: result[1]*/});
        });
    }

});

/*
router.get('/edit2', function(req, res){
   if(req.query.company_id == null) {
       res.send('A company id is required');
   }
   else {
       company_dal.getById(req.query.company_id, function(err, company){
           address_dal.getAll(function(err, address) {
               res.render('company/companyUpdate', {company: company[0], address: address});
           });
       });
   }

});
*/


router.get('/update', function(req, res) {
    Player_dal.update(req.query, function(err, result){
       res.redirect(302, '/Player/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.player_id == null) {
        res.send('player_id is null');
    }
    else {
         Player_dal.delete(req.query.player_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/Player/all');
             }
         });
    }
});

module.exports = router;
