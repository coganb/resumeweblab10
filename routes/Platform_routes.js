var express = require('express');
var router = express.Router();
var Platform_dal = require('../model/Platform_dal');
// var Platform_type_dal = require('../model/Platform_type_dal');

// HERE
// View All companys
router.get('/all', function(req, res) {
    Platform_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('Platform/PlatformViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.Platform_id == null) {
        res.send('Platform_id is null');
    }
    else {
        Platform_dal.getById(req.query.Platform_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('Platform/PlatformViewById', {'result': result});
           }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    Platform_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('Platform/PlatformAdd', {'Platform_type': result});
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.Platform_type == "") {
        res.send('Platform Name must be provided.');
    }
    else if(req.query.price == "")
    {
        res.send('Platform price must be provided.');
    }
    else if(req.query.operating_system == "")
    {
        res.send('Platform operating_system must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        Platform_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/Platform/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.Platform_id == null) {
        res.send('A Platform id is required');
    }
    else {
        Platform_dal.edit(req.query.Platform_id, function(err, result){
            res.render('Platform/PlatformUpdate', {Platform: result[0][0]/*, address: result[1]*/});
        });
    }

});

/*
router.get('/edit2', function(req, res){
   if(req.query.company_id == null) {
       res.send('A company id is required');
   }
   else {
       company_dal.getById(req.query.company_id, function(err, company){
           address_dal.getAll(function(err, address) {
               res.render('company/companyUpdate', {company: company[0], address: address});
           });
       });
   }

});
*/


router.get('/update', function(req, res) {
    Platform_dal.update(req.query, function(err, result){
       res.redirect(302, '/Platform/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.Platform_id == null) {
        res.send('Platform_id is null');
    }
    else {
         Platform_dal.delete(req.query.Platform_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/Platform/all');
             }
         });
    }
});

module.exports = router;
