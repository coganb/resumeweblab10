var express = require('express');
var router = express.Router();
var Game_dal = require('../model/Game_dal');
// var g_name_dal = require('../model/g_name_dal');

// HERE
// View All companys
router.get('/all', function(req, res) {
    Game_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('Game/GameViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.game_id == null) {
        res.send('game_id is null');
    }
    else {
        Game_dal.getById(req.query.game_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('Game/GameViewById', {'result': result});
           }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    Game_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('Game/GameAdd', {'g_name': result});
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.g_name == "") {
        res.send('Game Name must be provided.');
    }
    else if(req.query.price == "")
    {
        res.send('Game price must be provided.');
    }
    else if(req.query.genre == "")
    {
        res.send('Game Genre must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        Game_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/Game/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.game_id == null) {
        res.send('A Game id is required');
    }
    else {
        Game_dal.edit(req.query.game_id, function(err, result){
            res.render('Game/GameUpdate', {Game: result[0][0]/*, address: result[1]*/});
        });
    }

});

/*
router.get('/edit2', function(req, res){
   if(req.query.company_id == null) {
       res.send('A company id is required');
   }
   else {
       company_dal.getById(req.query.company_id, function(err, company){
           address_dal.getAll(function(err, address) {
               res.render('company/companyUpdate', {company: company[0], address: address});
           });
       });
   }

});
*/


router.get('/update', function(req, res) {
    Game_dal.update(req.query, function(err, result){
       res.redirect(302, '/Game/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.game_id == null) {
        res.send('game_id is null');
    }
    else {
         Game_dal.delete(req.query.game_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/Game/all');
             }
         });
    }
});

module.exports = router;
