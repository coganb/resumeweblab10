var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = "SELECT characters.*, g.g_name from Characters characters" +
    " LEFT JOIN Game g ON g.game_id = characters.game_id;";

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};




exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO Characters (game_id, c_name, is_playable, is_killable) VALUES (?)';


    var queryData = [params.game_id, params.c_name, params.is_playable, params.is_killable];




    connection.query(query, [queryData], function(err, result) {
    callback(err,result)
    });/*

        //WE STOPPED HERE !!!
        // THEN USE THE COMPANY_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
        var Characters_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO company_address (company_id, address_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var companyAddressData = [];
        for(var i=0; i < params.address_id.length; i++) {
            companyAddressData.push([company_id, params.address_id[i]]);
        }

        // NOTE THE EXTRA [] AROUND companyAddressData
        connection.query(query, [companyAddressData], function(err, result){
            callback(err, result);
        });
    });
    */

};

exports.delete = function(character_id, callback) {
    var query = 'DELETE FROM Characters WHERE character_id = ?';
    var queryData = [character_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

/*
//declare the function so it can be used locally
var companyAddressInsert = function(company_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO company_address (company_id, address_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var companyAddressData = [];
    for(var i=0; i < addressIdArray.length; i++) {
        companyAddressData.push([company_id, addressIdArray[i]]);
    }
    connection.query(query, [companyAddressData], function(err, result){
        callback(err, result);
    });
};

//export the same function so it can be used by external callers
module.exports.companyAddressInsert = companyAddressInsert;

//declare the function so it can be used locally
var companyAddressDeleteAll = function(company_id, callback){
    var query = 'DELETE FROM company_address WHERE company_id = ?';
    var queryData = [company_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers

module.exports.companyAddressDeleteAll = companyAddressDeleteAll;
*/
exports.update = function(params, callback) {
    var query = 'UPDATE Characters SET Characters_price=? WHERE Characters_id = ?';
    var queryData = [params.Characters_price, params.Characters_id];

    connection.query(query, queryData, function(err, result) {
        callback(err,result);
    });
};

/*
    connection.query(query, queryData, function(err, result) {
        //delete company_address entries for this company
        companyAddressDeleteAll(params.company_id, function(err, result){

            if(params.address_id != null) {
                //insert company_address ids
                companyAddressInsert(params.company_id, params.address_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};
*/
/*  Stored procedure used in this example
     DROP PROCEDURE IF EXISTS company_getinfo;

     DELIMITER //
     CREATE PROCEDURE company_getinfo (company_id int)
     BEGIN

     SELECT * FROM company WHERE company_id = _company_id;

     SELECT a.*, s.company_id FROM address a
     LEFT JOIN company_address s on s.address_id = a.address_id AND company_id = _company_id;

     END //
     DELIMITER ;

     # Call the Stored Procedure
     CALL company_getinfo (4);

 */

exports.edit = function(Characters_id, callback) {
    var query = 'CALL Characters_getinfo(?)';
    var queryData = [Characters_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};